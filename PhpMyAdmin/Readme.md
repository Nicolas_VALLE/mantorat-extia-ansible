# TP Mariadb sous Ansible

## Objectifs

Plusieurs rendus sont attendus à la fin de ce TP :

- Installation de PhpMyAdmin depuis les sources
- Création de la configuration

Des varibales sont à utiliser pour définir le nom DNS pour joindre phpmyadmin, pour restreindre l'accès au site via des IP, pour définir les serveurs sur lesquel PhpMyadmin peut se connecter pour atteindre des bases de données.

Les étapes d'installation de l'outils sont disponibles [ici](https://docs.phpmyadmin.net/fr/latest/setup.html#quick-install-1)

## Comment s'y prendre ?

1. Créer le rôle grâce à la commande `ansible-galaxy`

2. Se positionner dans les tasks du rôle et créer deux fichiers. Le premier contiendera les étapes d'installation, le second les étapes de configuration.

3. L'installation de phpmyadmin se réalise en trois actions. La création d'un dossier allant contenir l'archive PhpMyAdmin, le téléchagement et le dezippage de l'archive et l'installation des paquets php nécessaires au bon fonctionnement de l'application. Pour vous aidez, regarder les modules `ansible.builtin.file`, `ansible.builtin.unarchive` et `ansible.builtin.apt`.

    Les paquets php à télécharger sont les suvants :

    - libapache2-mod-php7.3
    - php7.3
    - php7.3-common
    - php7.3-curl
    - php7.3-mysqli
    - php7.3-mbstring

4. L'installation des paquets php necessite le redémarrage du service apache2. Créer un `handler` à la fin de l'installation des paquets.

5. L'installation est terminée, vous allez donc passer à la configuration. Pour commencer, le template ci-dessous vous permettra de créer un vhost pour apache2, il est installer dans le repertoire `/etc/apache2/site-available`.

    ```ini
    <VirtualHost *:80>
        ServerName {{ phpmyadmin_domain_name }}
        DocumentRoot /usr/share/phpmyadmin
        <DirectoryMatch "(templates|libraries|\.(svn|git|hg|bzr|cvs))/">
            Require all denied
        </DirectoryMatch>

        <Directory /usr/share/phpmyadmin/>
            Options -Indexes +FollowSymLinks +MultiViews
            AllowOverride All
        {% for ip in phpmyadmin_authorized_ip %}
            Require ip {{ ip }}
        {% endfor %}
            Require all denied
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/phpmyadmin.error.log
        CustomLog ${APACHE_LOG_DIR}/phpmyadmin.access.log combined
    </VirtualHost>
    ```

    Lisez le et repérez les variables nécessaire à son bon fonctionnement. Déclarer ces variables dans le fichier `host_vars/ansible-client.yml`. Utilisez le module `ansible.builtin.template` pour traiter le template dans le rôle.

6. Le site est disponible dans le dossier `/etc/apache2/site-available`, la commande `a2ensite phpmyadmin.conf` permet d'activer le site afin qu'il soit servie par apache. Cette commande ne doit être lancée que si une modification du template à été réalisée. Une nouvelle tâche `ansible.builtin.command` liée à un `when` et à un `register` de la tâche précédente permet de lancer correctement le tout.

7. Une modification de configuration d'apache nécessite obligatoirement son redémarrage. Positionner un `handler` à la fin de la tâche précédente.

8. Un paramètre de sécurité est attendu le fichier de configuration PhpMyAdmin. Ce paramètre se nomme blowfish. Voici les étapes pour le générer et l'enregister dans une variable :

    ```yaml
    - name: Generate blowfish_secret
    ansible.builtin.shell: head /dev/urandom | tr -dc 'A-Za-z0-9' | head -c 32 ; echo ''
    register: blowfish
    changed_when: False

    - ansible.builtin.set_fact:
        blowfish_secret: "{{ blowfish.stdout }}"
    ```

    La module `shell` permet d'utiliser les commandes linux comme dans un terminal. Nous générons ici une chaine aléatoire de 32 caractères qui sera ensuite enregistrée dans la variable blowfish. Cette variable est une liste contenant toutes les informations d'exectution de la commande shell. Ici, seul la sortie standard nous intéresse et, dans le deuxième tâche, elle est récupérée et stockée dans la variable blowfish_secret.

9. Un deuxième template est à déployer dans le repertoire `/usr/share/phpmyadmin/config.inc.php`. Il contient les informations de configuration de PhpMyAdmin :

    ```php
    <?php

    /* Servers configuration */
    $i = 0;
    {% for host in phpmyadmin_hosts_list %}
    /* Server: {{ host.name | default('localhost') }} */
    $i++;
    $cfg['Servers'][$i]['verbose'] = '{{ host.name }}';
    $cfg['Servers'][$i]['host'] = '{{ host.ip | default('localhost') }}';
    $cfg['Servers'][$i]['port'] = '{{ host.port | default('3306') }}';
    $cfg['Servers'][$i]['socket'] = '';
    $cfg['Servers'][$i]['auth_type'] = 'cookie';
    $cfg['Servers'][$i]['user'] = '';
    $cfg['Servers'][$i]['password'] = '';
    $cfg['Servers'][$i]['AllowNoPassword'] = false;
    $cfg['Servers'][$i]['AllowRoot'] = false;

    {% endfor %}

    /* End of servers configuration */

    $cfg['blowfish_secret'] = "{{ blowfish_secret }}";
    $cfg['UploadDir'] = '';
    $cfg['SaveDir'] = '';
    $cfg['BZipDump'] = false;
    $cfg['DefaultLang'] = 'en';
    $cfg['ServerDefault'] = 1;
    $cfg['TempDir'] = '/tmp';
    $cfg['VersionCheck'] = false;
    $cfg['ShowPhpInfo'] = false;
    $cfg['PmaNoRelation_DisableWarning'] = true;
    $cfg['ShowServerInfo'] = false;
    $cfg['NavigationTreeEnableGrouping'] = false;
    ?>
    ```

    Repérez les variables utilisées lors dans ce template et ajoutez les dans le fichier `host_vars/ansible-client.yml`.

10. PhpMyAdmin peut-être installé de manière intéractive en appelant des fichiers php d'administration. Ils sont une faille et doivent être supprimés. Utiliser le module `file` et supprimer les fichiers `/usr/share/phpmyadmin/test` et `/usr/share/phpmyadmin/setup`

11. C'est terminé ! Ou presque :) Au vu du nombre de modification faite dans le repertoire `/usr/share/phpmyadmin`, il serait dommage de supprimer cette configuration leur de la phase d'installation et de PhpMyadmin. Retournez dans le fichier `tasks/main.yml` et ajoutez comme première tâche un moyen de vérifier si le dossier `/usr/share/phpmyadmin` existe. Penchez-vous du côté du module `ansible.builtin.stat`. Si le dossier existe alors utiliser `when` afin de ne pas lancer l'installation de phpmyadmin. La configuration peut, elle, se réaliser car il est possible que de nouveaux serveurs de BDD soient à y ajouter.
