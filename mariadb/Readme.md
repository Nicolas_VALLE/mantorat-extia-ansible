# TP Mariadb sous Ansible

## Objectifs

Plusieurs rendus sont attendus à la fin de ce TP :

- Installation de Mariadb sur le serveur ansible-client via un rôle
- Création d'une ou plusieurs bases de données
- Ajout d'utilisateur ayant les droits sur les bases précédemment créée

Les utilisateurs et des bases de données à créer devront être listés dans des variables présentes dans le host_vars.

## Comment s'y prendre ?

1. Comme dans le TP précédent, il faut créer un role mariadb. Le rôle se créer via la commande `ansible-galaxy`.

2. Créer un playbook faisant appel au role mariadb.

3. Afin de simplifier la visibilité du code, il est d'usage de realiser des **include** des fichiers yaml contenant du code. Par exemple, dans le fichier `role/mariadb/tasks/main.yml`, ajouter les lignes suivantes :

    ```yaml
    ---
    - import_tasks: install_mariadb.yml
    ```

    Créer ensuite le fichier `install_mariadb.yml` dans le répertoire tasks, la suite du script Ansible se fera dans ce document pour l'instant.

4. Dans `install_mariadb.yml`, deux étapes sont nécessaires pour installer et paramétrer mysql. L'installation se fait via le module `ansible.builtin.apt`, la documentation est disponible sur internet.

    Trois paquets sont à installer :

    - mariadb-client
    - mariadb-server
    - python-mysqldb

5. La configuration de mariadb se fait via un fichier de configuration à placer dans le repertoire `/etc/mysql/mariadb.conf.d/50-server.cnf`. Voici à quoi il peut ressembler :

    ```ini
    # The MariaDB database server configuration file.
    #

    [mysqld]
    user        = mysql
    pid_file    = /var/run/mysqld/mysqld.pid
    socket      = /var/run/mysqld/mysqld.sock
    port        = 3306
    basedir     = /usr

    skip-external-locking
    key_buffer_size                 = 16M
    max_allowed_packet              = 16M
    thread_stack                    = 192K
    thread_cache_size               = 8
    long_query_time                 = 2
    max_binlog_size                 = 100M
    bind_address                    = 0.0.0.0
    general_log_file                = /var/log/mysql/mysql.log
    log_error                       = /var/log/mysql/error.log
    slow-query-log-file             = /var/log/mysql/mysql-slow.log
    innodb_flush_method             = O_DIRECT
    server-id                       = 1
    datadir                         = /var/lib/mysql
    tmpdir                          = /tmp
    log_bin                         = /var/log/mysql/mysql-bin.log
    max_connections                 = 151
    innodb_buffer_pool_size         = 8M
    innodb_buffer_pool_instances    = 8
    innodb_log_buffer_size          = 8M
    innodb_log_file_size            = 256M
    ```

    Copier/coller cette configuration et placer la dans le fichier `roles/mariadb/files/50-server.cnf`. Utiliser ensuite le module `ansible.builtin.copy` pour la déposer sur le serveur distant.

6. L'installation de mariadb est terminée, mais un détail reste à régler. Si une modification est réalisée dans la configuration de mariadb, il faut redémarrer le service. Créer un `handler` dans la même logique que celui créé pour le role apache2.

7. L'installation de mariadb est terminée, il est maintenant temps de gérer la partie utilisateur. Retourner dans le fichier `tasks/main.yml` et importer un nouveau fichier correspondant aux tâches à réaliser pour créer les utilisateurs.

8. Lisez la documentation du module `community.mysql.mysql_user` et essayer de l'utiliser pour créer votre premier utilisateur. Un plus serait créer une liste d'utilisateur dans le fichier `host_vars/ansible-client.yml`. Voici à quoi la déclaration de ces variables pourraient ressembler :

    ```yaml
    ---
    bdd_user:
    - { name: user1, password: P@ssword, host: 'localhost', priv: 'bdd1.*:ALL' }
    - { name: user2, password: P@ssword, host: 'localhost', priv: 'bdd2.*:ALL' }
    ```

    Il est possible de boucler sur la liste des utilisateurs en s'inspirant des instructions suivantes :

    ```yaml
    - name: Add db account
    community.mysql.mysql_user:
        name: "{{ item.name }}"
    loop: "{{ bdd_user |flatten (levels=1) }}"
    ```

9. Oups, nous venons de mettre un bug dans notre script. En effet, si nous lançons le script mais qu'aucun utilisateur n'est déclaré, une erreur apparait à la fin de l'execution du script. Pour se prémunir de cette erreur, utiliser la condition `when` pour vérifier que la variable bdd_user est bien définie.

10. La gestion des utilisateurs est terminée, il est enfin temps de créer notre première base de données. Pour se faire, et comme les autres fois, retourner dans le fichier `tasks/main.yml` et rajouter une ligne d'import d'un nouveau fichier. Les tâches pour créer la base de données seront à mettre ici.

11. Lisez la documentation du module `community.mysql.mysql_db` et, comme pour la partie utilisateur, ajoutez les variables suivantes :

    ```yaml
    bdd_name:
    - bdd1
    - bdd2
    ```

12. Bravo, nous sommes à la fin de notre TP mariadb ! Un dernière chose, lors de la création des utilisateurs bdd, vous avez indiqué un secret en clair dans la configuration. Pour corriger cette faille de sécurité, comme dans le TP apache2, il est possible d'utiliser ansible-vault.

    ```bash
    ansible-vault create <chemin pour le stockage des secrets>
    ```

    Indiquer un mot de passe pour chiffrer le fichier vault et créer une variable contenant les mots de passe des utilisateurs de base de données.

    Corriger ensuite la variable `bdd_user` pour utiliser le vault:

    ```yaml
    ---
    bdd_user:
    - { name: user1, password: "{{vault_password_user1}}", host: 'localhost', priv: 'bdd1.*:ALL' }
    - { name: user2, password: "{{vault_password_user2}}", host: 'localhost', priv: 'bdd2.*:ALL' }
    ```

    Enfin, pour que cette variable soit connue par ansible lors de l'appel du playbook, ajoutez dans le fichier `playbooks/anisble-client.yml` la présence d'un nouveau fichier de variables :

    ```yml
    vars_files:
      - <path du chemin pour le stockage des secrets>
    ```

---

> Vous avez terminé et il vous reste du temps ? Un dernier TP est disponible. Pour compléter la stack apache2 et mariadb, nous vous proposons d'installer PhpMyAdmin.
