# Lancement de l'environnement de TP

## Pré-requis

Trois pré-requis sont nécessaires pour la réalisation des TP :

1. [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
2. [Vagrant](https://www.vagrantup.com/downloads)
3. Un PC (windows ou linux) avec 6 à 8 Go de RAM

> Vagrant **ne fonctionne pas correctement** dans un chemin contenant un dossier avec accent ou avec un espace. Si vous êtes dans ce cas là, placer les documents fourni dans un dossier à la racine du `C:\` (sous windows) ou dans `/` (sous linux). Vous pouvez également essayer la commande suivante sous Windows : `chcp 1252`.

## Création des machines virtuelles

### Initialisation

La création des machines virtuelles se fait via le lancement du fichier Vagrant présent à la racine du dépôt :

```bash
# Installation d'un plugin Vagrant pour le redémarrage des serveurs pendant le provisionnement
vagrant plugin install vagrant-reload

# Création des VM
vagrant up

# Création de snapshot des VM
vagrant snapshot push

# Suppression des VM
vagrant destroy 
```

Deux machines seront créées :

* le serveur **ansible**
  * **hostname** : ansible-serveur
  * **IP** : 192.168.56.10
* le serveur **client**
  * **hostname** : ansible-client
  * **IP** : 192.168.56.11

### Accès

#### Identifiants

L'identifiants de connexion est **vagrant** et son mot de passe est **vagrant**.

#### Connexion

Il est possible de se connecter de deux manières différentes aux serveurs :

**SSH** :

```bash
vagrant ssh <hostname de la vm>
```

**Console** :

En lançant virtualbox, vous pouvez accéder aux VMs directement en console.
Une **interface graphique** est disponible sur le serveur **ansible**. Un fichier est présent sur le **bureau** et permet de lancer **VsCode** avec un projet pointant vers le répertoire `/etc/ansible`.

### Fin d'un TP

A la fin de chaque TP, les machines peuvent-être restaurées dans leur état d'origine (état juste après le `vagrant snapshot push`). **Tout** le code écrit sera supprimé.

```bash
vagrant snapshot pop
```

### Fin du mentorat

A la fin de la période de mantora, la commande suivante permet de supprimer toutes les VM :

```bash
vagrant destroy --force
```

### Bug

* L'accès en console peut **bugger** lors de la première connexion : changer la taille de la fenêtre pour que l'affichage se corrige.
* En cas de lenteur lors de l'utilisation de **VsCode**, redémarrer la VM.
* En cas de destruction des VM sur certaines machines hôtes **Windows**, grâce à la commande `vagrant destroy`, des dossiers résiduels peuvent rester dans le chemin `C:\Users\<user>\VirtualBox VMs\`. Penser à les supprimer pour éviter touts erreurs lors d'un prochain `vagrant up`.
* Sous **Windows**, Si vous rencontrez des erreurs kernels, augmenter le nombre de vCPU à **2** par VM.
* Si vous rencontrez une erreur type `The box 'generic/debian10' could not be found or could not be accessed in the remote catalog`, [télécharger la box vagrant ici](https://app.vagrantup.com/generic/boxes/debian10) et en sélectionnant le **provider VirtualBox**. Se positionner dans le répertoire dans lequel le fichier a été téléchargé et lancer `vagrant box add generic/debian10 ./<nom du fichier téléchargé>`. Relancer la commande `vagrant up`
